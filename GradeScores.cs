﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;

namespace grade_scores
{
    public class GradeScores
    {
        public void GetGradeScores(string FilePath)
        {
            DataTable dt = new DataTable();

            DataColumn LastName = new DataColumn("LastName");
            LastName.DataType = typeof(String);

            DataColumn FirstName = new DataColumn("FirstName");
            FirstName.DataType = typeof(String);

            DataColumn Score = new DataColumn("Score");
            Score.DataType = typeof(Int32);

            dt.Columns.Add(LastName);
            dt.Columns.Add(FirstName);
            dt.Columns.Add(Score);


            String[] lines = File.ReadAllLines(FilePath);

            foreach (String leagueScore in lines)
            {
                String[] split = leagueScore.Split(',');
                DataRow dr = dt.NewRow();
                if (split.Length > 2)
                {
                    dr["LastName"] = split[0].Trim();
                    dr["FirstName"] = split[1].Trim();
                    dr["Score"] = split[2].Trim();
                    dt.Rows.Add(dr);
                }
            }
            dt.DefaultView.Sort = "Score DESC,LastName,FirstName ASC";

            dt = dt.DefaultView.ToTable(false);


            string outputpath = FilePath.Substring(0, FilePath.LastIndexOf(@"\"));

            string path = outputpath + @"\names-graded.txt";

            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    sw.WriteLine("{0}, {1}, {2}", dr["LastName"], dr["FirstName"], dr["Score"]);
                    Console.WriteLine("{0}, {1}, {2}", dr["LastName"], dr["FirstName"], dr["Score"]);
                }
                Console.WriteLine("Finished: created names-graded.txt");
            }

        }
    }
}
